import Page from 'components/Page';
//import UsersTable from 'components/Card';
import React from 'react';
import { Card, CardBody, CardHeader, Col, Row, Table } from 'reactstrap';

const tableTypes = [''];

const UsersPage = () => {
  return (
    <Page
      title="Users"
      breadcrumbs={[{ name: 'Users', active: true }]}
      className="UsersPage"
    >
      {tableTypes.map((tableType, index) => (
        <Row key={index}>
          <Col>
            <Card className="mb-3">
              <CardHeader>Registered Users </CardHeader>
              <CardBody>
                <Row>
                  <Col>

                    <Card body>
                      <Table responsive {...{ ['hover']: true }}>
                        
                        
                        <thead>
                          <tr rowSpan="4">
                          <th className="text-center"> Admins</th>
                          </tr>

                          <tr>
                            <th>#</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Username</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <th scope="row">1</th>
                            <td>Mark</td>
                            <td>Otto</td>
                            <td>@mdo</td>
                          </tr>
                          <tr>
                            <th scope="row">2</th>
                            <td>Jacob</td>
                            <td>Thornton</td>
                            <td>@fat</td>
                          </tr>
                          <tr>
                            <th scope="row">3</th>
                            <td>Larry</td>
                            <td>the Bird</td>
                            <td>@twitter</td>
                          </tr>
                        </tbody>
                      </Table>
                    </Card>
                  </Col>

                  
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>
      ))}
      
    </Page>
  );
};

export default UsersPage;
